/*Copyright (c) 2016 Michael Abadi Santoso

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a cluster containing only one item
 * 
 * @author hadoop
 */

public abstract class ClusteringItem extends Cluster implements Serializable{
   
   private String namaItem = null; 
   private Integer posisi = null;
   private Double clusterMinDist = null;
   private Double umur = null;
   private Double totalAnak = null;
   private Double totalWanita = null;
   private Double totalPria = null;
   private Double totalSepatu = null;
      
//private Double[][] distMap = null;
    //private List<DistanceMap> distMap = null;
   
   @Override
   public void setName(String name){
       this.setNamaItem(name);
   }
   
   @Override
   public String getName(){
       return this.getNamaItem();
   }
   
   
   public void setUmur(Double umur){
       this.umur = umur;
   }
   
   public Double getUmur(){
       return this.umur;
   }
   
   public void setTotalAnak(Double totalAnak){
       this.totalAnak = totalAnak;
   }
   
   public Double getTotalAnak(){
       return this.totalAnak;
   }
   public void setTotalWanita(Double totalWanita){
       this.totalWanita = totalWanita;
   }
   
   public Double getTotalWanita(){
       return this.totalWanita;
   }
   public void setTotalPria(Double totalPria){
       this.totalPria = totalPria;
   }
   
   public Double getTotalPria(){
       return this.totalPria;
   }
   public void setTotalSepatu(Double totalSepatu){
       this.totalSepatu = totalSepatu;
   }
   
   public Double getTotalSepatu(){
       return this.totalSepatu;
   }
   
   @Override
   public List<ClusteringItem> getItems() {
      ArrayList<ClusteringItem> result = new ArrayList<>();
      result.add(this);
      return result;
   }

   /** {@inheritDoc} */
   @Override
   public List<Cluster> getChildren() {
      return new ArrayList<>();
   }
   @Override
    public Double getClusterMinDist(){
       return this.clusterMinDist;
    }

    @Override
    public void setClusterMinDist(Double clusterMinDist){
       this.clusterMinDist = clusterMinDist;
    }
   
   public void setNamaItem(String namaItem){
       this.namaItem = namaItem;
   }
   
   public String getNamaItem(){
       return this.namaItem;
   }
   
//   public void setDistMap(Double[][] distMap){
//       this.distMap = distMap;
//   }
   
//   public Double[][] getDistMap(){
//       return this.distMap;
//   }
   
   public void setPosisi(Integer posisi){
       this.posisi = posisi;
   }
   
   public Integer getPosisi(){
       return this.posisi;
   }
   
   /*public void setDistMap(List<DistanceMap> distMap){
       this.distMap = distMap;
   }
   
   public List<DistanceMap> getDistMap(){
       return this.distMap;
   }*/
   
}
