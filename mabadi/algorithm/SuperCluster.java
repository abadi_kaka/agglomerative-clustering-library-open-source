/*Copyright (c) 2016 Michael Abadi Santoso

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hadoop
 */
public class SuperCluster extends Cluster implements Serializable{
    private List<Cluster> children = new ArrayList<>();
    private Double clusterMinDist = null;
    private String name = null;
   public SuperCluster() {

   }
   
   public SuperCluster(Cluster first, Cluster second) {
      children.add(first);
      children.add(second);
   }
   
   public void setName(String name){
       String nama = "Cluster #" + name;
       this.name = nama;
   
   }
   
   public String getName(){
       return this.name;
   }
    public List<ClusteringItem> getItems() {
      List<ClusteringItem> result = new ArrayList<>();
      for (Cluster subCluster : children) {
         result.addAll(subCluster.getItems());
      }
      return result;
    }
    
    public List<Cluster> getChildren() {
      return children;
    }
      
    @Override
    public Double getClusterMinDist(){
       return this.clusterMinDist;
    }

    @Override
    public void setClusterMinDist(Double clusterMinDist){
       this.clusterMinDist = clusterMinDist;
    }

    
}
